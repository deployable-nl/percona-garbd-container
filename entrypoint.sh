#!/bin/sh
set -e

export GALERA_NODES=$(dig "$CLUSTER_NAME"-pxc-unready.pxc.svc.cluster.local +short | xargs | sed -e 's/ /,/')
export GALERA_GROUP="${CLUSTER_NAME}-pxc"

exec "$@"
