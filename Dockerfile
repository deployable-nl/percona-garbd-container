FROM ubuntu:20.04

ADD sources.list /etc/apt/sources.list
ADD entrypoint.sh /entrypoint.sh
SHELL ["/bin/bash", "-xeo", "pipefail", "-c"]

RUN apt-get update;\
    apt-get install -y --no-install-recommends dnsutils curl ca-certificates gnupg lsb-release;\
    curl https://repo.percona.com/apt/percona-release_latest.focal_all.deb -o /tmp/percona-release_latest.focal_all.deb;\
    dpkg -i /tmp/percona-release_latest.focal_all.deb;\
    percona-release setup pdpxc-8.0;\
    apt-get install -y --no-install-recommends percona-xtradb-cluster-garbd;\
    apt-get purge --autoremove -y gnupg curl ca-certificates lsb-release;\
    rm -rf /var/lib/apt/lists/* /tmp/percona-release_latest.focal_all.deb;\
    chmod 700 /entrypoint.sh;

ENTRYPOINT ["/entrypoint.sh"]
CMD garbd -a gcomm://${GALERA_NODES} -g ${GALERA_GROUP}
